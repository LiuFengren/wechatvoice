package com.huolongluo.wechatvioce.widget;

import android.content.Context;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.huolongluo.wechatvioce.R;
import com.huolongluo.wechatvioce.widget1.VoiceView;
import com.huolongluo.wechatvioce.widget2.VoiceView2;

public class SendVioceRelativeLayout extends RelativeLayout {
    private static final String TAG = "SendVioceRelativeLayout";

    //改成VoiceView则id也一样改成voice_view
    //改成VoiceView2则id也一样改成voice_view2
//    private VoiceView voice_view;
    private VoiceView2 voice_view;


    private Button btn_send_voice;

    public SendVioceRelativeLayout(Context context) {
        this(context, null);
    }

    public SendVioceRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SendVioceRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        //改成VoiceView则id也一样改成voice_view
        //改成VoiceView2则id也一样改成voice_view2
//        voice_view = findViewById(R.id.voice_view);
        voice_view = findViewById(R.id.voice_view2);

        btn_send_voice = findViewById(R.id.btn_send_voice);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.e(TAG, "onInterceptTouchEvent: 语音发送父组件");
        int action = ev.getAction();
        if (action == MotionEvent.ACTION_DOWN) {
            init();
            vibrator(getContext());//震动
            voice_view.setVisibility(VISIBLE);
            voice_view.doStart();
            btn_send_voice.setVisibility(GONE);
        } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
            voice_view.doDefault();
            btn_send_voice.setVisibility(VISIBLE);
        }


        return false;
    }

    /**
     * 震动
     */
    private void vibrator(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        long[] patter = {0, 100};
        vibrator.vibrate(patter, -1);
    }

//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        int action = event.getAction();
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//                Log.e(TAG, "onTouch: 按下");
//                voice_view.setVisibility(View.VISIBLE);
//                break;
//            case MotionEvent.ACTION_MOVE:
//                Log.e(TAG, "onTouch: 移动");
//                break;
//            case MotionEvent.ACTION_UP:
//                voice_view.setVisibility(View.INVISIBLE);
//                Log.e(TAG, "onTouch: 抬起");
//                break;
//        }
//        return super.onTouchEvent(event);
//    }
}

package com.huolongluo.wechatvioce.widget2;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.IntDef;
import androidx.annotation.Nullable;

import com.huolongluo.wechatvioce.R;
import com.huolongluo.wechatvioce.widget1.VoiceBubble;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 气泡
 * */
public class VoiceBubble2 extends View {
    private static final String TAG = "VoiceBubble";

    private float triangle_width;//三角箭头的宽度
    private float triangle_height;//三角箭头的高度
    private float triangle_margin_start;//三角箭头的高度（默认为0，即便居中）

    private float bubble_default_width;//显示默认中间时的气泡宽度
    private float bubble_default_height;//显示默认中间时的气泡高度
    private float bubble_cancel_margin_start;//显示取消时的气泡的左边距
    private float bubble_cancel_width;//显示取消时的气泡宽度
    private float bubble_cancel_height;//显示取消时的气泡高度
    private float bubble_transform_margin_top;//显示转文字时的气泡上边距
    private float bubble_transform_width;//显示转文字时的气泡宽度
    private float bubble_transform_height;//显示转文字时的气泡高度

    private float wave_default_width;//显示默认中间时的波浪宽度
    private float wave_default_height;//显示默认中间时的波浪高度
    private float wave_cancel_width;//显示取消时的波浪宽度
    private float wave_cancel_height;//显示取消时的波浪高度
    private float wave_transform_width;//显示转文字时的波浪宽度
    private float wave_transform_height;//显示转文字时的波浪高度

    private float wave_margin_end;//显示转文字时波浪距离当前组件右边的距离
    private float wave_margin_bottom;//显示转文字时波浪距离当前组件下边的距离
    /**********************/
    // 音波线宽度
    private final int VOICE_LINE_WIDTH = 4;
    private Paint redPaint;
    private Paint greenPaint;
    private Paint writePaint;//Pop中的白色音波
    private TextPaint textPaint;//转文字时的文字画笔
    private RectF defaultRectF;//中间默认显示的矩形
    private RectF translateRectF;//语音识别显示的矩形
    private RectF cancelRectF;//取消显示的矩形
    private Path trianglePath;//三角形

    private float textWidth;//文字宽度
    private float textHeight;//文字高度
    private StaticLayout staticLayout;
    private int CURRENT_TYPE = SHOW_TYPE.TYPE_CENTER;//当前显示的”中间的POP“，还是”取消“，还是”转文字“

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({
            VoiceBubble2.SHOW_TYPE.TYPE_CENTER,
            VoiceBubble2.SHOW_TYPE.TYPE_CANCEL,
            VoiceBubble2.SHOW_TYPE.TYPE_TRANSLATE
    })
    public @interface SHOW_TYPE {
        int TYPE_CENTER = 101;
        int TYPE_CANCEL = 102;
        int TYPE_TRANSLATE = 103;
    }

    public VoiceBubble2(Context context) {
        this(context, null);
    }

    public VoiceBubble2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.VoiceBubble2);
        triangle_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_triangle_width, 30);
        triangle_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_triangle_height, 35);
        triangle_margin_start = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_triangle_margin_start, 0);

        bubble_default_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_default_width, 600);
        bubble_default_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_default_height, 200);

        bubble_cancel_margin_start = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_cancel_margin_start, 20);
        bubble_cancel_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_cancel_width, 300);
        bubble_cancel_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_cancel_height, 200);

        bubble_transform_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_transform_width, 900);
        bubble_transform_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_transform_height, 240);

        wave_default_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_default_width, 60);
        wave_default_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_default_height, 30);
        wave_cancel_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_cancel_width, 60);
        wave_cancel_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_cancel_height, 30);
        wave_transform_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_transform_width, 60);
        wave_transform_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_transform_height, 30);

        wave_margin_end = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_margin_end, 30);
        wave_margin_bottom = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_margin_bottom, 30);

        trianglePath = new Path();

        typedArray.recycle();//将TypedArray对象回收
        init();
    }

//    public VoiceBubble2(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.VoiceBubble2);
//        triangle_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_triangle_width, 10);
//        triangle_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_triangle_height, 10);
//        triangle_margin_start = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_triangle_margin_start, 0);
//
//        bubble_default_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_default_width, 600);
//        bubble_default_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_default_height, 200);
//        bubble_cancel_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_cancel_width, 300);
//        bubble_cancel_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_cancel_height, 140);
//        bubble_transform_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_transform_width, 300);
//        bubble_transform_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_bubble_transform_height, 140);
//
//        wave_default_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_default_width, 60);
//        wave_default_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_default_height, 30);
//        wave_cancel_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_cancel_width, 60);
//        wave_cancel_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_cancel_height, 30);
//        wave_transform_width = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_transform_width, 60);
//        wave_transform_height = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_transform_height, 30);
//
//        wave_margin_end = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_margin_end, 30);
//        wave_margin_bottom = typedArray.getDimensionPixelSize(R.styleable.VoiceBubble2_wave_margin_bottom, 30);
//
//        trianglePath = new Path();
//
//        typedArray.recycle();//将TypedArray对象回收
//        init();
//    }

    private void init() {
        greenPaint = new Paint();
        greenPaint.setAntiAlias(true);
        greenPaint.setColor(0xFF00cb32);
        greenPaint.setStyle(Paint.Style.FILL);
        redPaint = new Paint();
        redPaint.setAntiAlias(true);
        redPaint.setColor(0xFFcb3a35);
        redPaint.setStyle(Paint.Style.FILL);
        writePaint = new Paint();
        writePaint.setAntiAlias(true);
        writePaint.setColor(0xFFffffff);
        writePaint.setStyle(Paint.Style.FILL);
        writePaint.setStrokeWidth(VOICE_LINE_WIDTH);
        textPaint = new TextPaint();
        textPaint.setAntiAlias(true);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(50f);
        textPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = 906;
        Log.e(TAG, "VoiceBubble2: 当前组件宽度：" + measuredWidth + "  高度：" + measuredHeight + " ====================>：" + ((measuredHeight - bubble_default_width) / 2) + "  bubble_default_width:" + bubble_default_width);
        if (defaultRectF == null) {
            defaultRectF = new RectF((measuredWidth - bubble_default_width) / 2, measuredHeight - bubble_default_height - triangle_height, ((measuredWidth - bubble_default_width) / 2) + bubble_default_width, measuredHeight - triangle_height);
            cancelRectF = new RectF(bubble_cancel_margin_start, measuredHeight - bubble_cancel_height - triangle_height, bubble_cancel_margin_start + bubble_cancel_width, measuredHeight - triangle_height);
        }
        translateRectF = new RectF((measuredWidth - bubble_transform_width) / 2, measuredHeight - bubble_transform_height - triangle_height, ((measuredWidth - bubble_transform_width) / 2) + bubble_transform_width, measuredHeight - triangle_height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.e(TAG, "onDraw: 当前的类型：" + CURRENT_TYPE);
        switch (CURRENT_TYPE) {
            case SHOW_TYPE.TYPE_CENTER:
                canvas.drawRoundRect(defaultRectF, 36, 36, greenPaint);
                break;
            case SHOW_TYPE.TYPE_CANCEL:
                canvas.drawRoundRect(cancelRectF, 36, 36, greenPaint);
                break;
            case SHOW_TYPE.TYPE_TRANSLATE:
                canvas.drawRoundRect(translateRectF, 36, 36, greenPaint);
                break;
        }

        canvas.drawPath(trianglePath, greenPaint);

        if (CURRENT_TYPE == VoiceBubble.SHOW_TYPE.TYPE_TRANSLATE) {
//            float textWidth = translateRectF.right - translateRectF.left;
//            StaticLayout staticLayout1 = new StaticLayout("简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao！！！", textPaint2, (int) textWidth,
//                    Layout.Alignment.ALIGN_NORMAL, 1, 10, true);
//            Log.e(TAG, "onDraw: 文字高度：" + staticLayout1.getHeight());
            canvas.translate(translateRectF.left + 10, translateRectF.top);
            staticLayout.draw(canvas);
//            canvas.drawText("", translateRectF.left, translateRectF.top + 50, textPaint);
        }
    }

    public void setShowType(@VoiceBubble2.SHOW_TYPE int type, String showStr) {
        textWidth = translateRectF.right - translateRectF.left - 20;
        staticLayout = new StaticLayout(showStr, textPaint, (int) textWidth,
                Layout.Alignment.ALIGN_NORMAL, 1, 10, true);
        textHeight = staticLayout.getHeight();
        textWidth = staticLayout.getWidth();

        bubble_transform_height = textHeight + 60;

        CURRENT_TYPE = type;
        trianglePath.reset();
        switch (CURRENT_TYPE) {
            case SHOW_TYPE.TYPE_CENTER:
//                defaultRectF.left = (getWidth() - bubble_default_width) / 2;
//                defaultRectF.top = getHeight();
//                defaultRectF.right = defaultRectF.left + bubble_default_width;
//                defaultRectF.bottom = defaultRectF.top + bubble_default_height;
                trianglePath.moveTo((defaultRectF.right) - (bubble_default_width / 2) - (triangle_width / 2), defaultRectF.bottom);
                trianglePath.lineTo((defaultRectF.right) - (bubble_default_width / 2), defaultRectF.bottom + triangle_height);
                trianglePath.lineTo((defaultRectF.right) - (bubble_default_width / 2) + (triangle_width / 2), defaultRectF.bottom);
                invalidate();
                break;
            case SHOW_TYPE.TYPE_CANCEL:
                trianglePath.moveTo((cancelRectF.right) - (bubble_cancel_width / 2) - (triangle_width / 2), cancelRectF.bottom);
                trianglePath.lineTo((cancelRectF.right) - (bubble_cancel_width / 2), cancelRectF.bottom + triangle_height);
                trianglePath.lineTo((cancelRectF.right) - (bubble_cancel_width / 2) + (triangle_width / 2), cancelRectF.bottom);
                invalidate();
                break;
            case SHOW_TYPE.TYPE_TRANSLATE:
                trianglePath.moveTo((translateRectF.right) - 100 - (triangle_width / 2), translateRectF.bottom);
                trianglePath.lineTo((translateRectF.right) - 100, translateRectF.bottom + triangle_height);
                trianglePath.lineTo((translateRectF.right) - 100 + (triangle_width / 2), translateRectF.bottom);
                invalidate();
                break;
        }


//        textWidth = translateRectF.right - translateRectF.left - 20;
//        staticLayout = new StaticLayout("简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao！！！", textPaint2, (int) textWidth,
//                Layout.Alignment.ALIGN_NORMAL, 1, 10, true);
//        textHeight = staticLayout.getHeight();
//        textWidth = staticLayout.getWidth();
//        Log.e(TAG, "setShowType: 文字高度：" + staticLayout.getHeight() + "  文字宽度：" + textWidth);
//
//        switch (type) {
//            case WeChatVoiceBubble.SHOW_TYPE.TYPE_CENTER://中间默认
//                targetRectF = defaultRectF;
//                targetTrianglePoints = centerTrianglePoints;
//                currPaint = greenPaint;
//                targetVoiceRectF = centerVoiceRectF;
//                recording = true;
//                break;
//            case WeChatVoiceBubble.SHOW_TYPE.TYPE_CANCEL://取消
//                targetRectF = cancelRectF;
//                targetTrianglePoints = cancelTrianglePoints;
//                currPaint = redPaint;
//                targetVoiceRectF = cancelVoiceRectF;
//                recording = false;
//                break;
//            case WeChatVoiceBubble.SHOW_TYPE.TYPE_TRANSLATE://识别
//                targetRectF = translateRectF;
//                targetTrianglePoints = translateTrianglePoints;
//                currPaint = greenPaint;
//                targetVoiceRectF = translateVoiceRectF;
//                recording = false;
//                break;
//            default:
//        }
//        int num = 10;
//        deltaTopY = (targetRectF.top - currRectF.top) / num;
//        deltaLeftX = (targetRectF.left - currRectF.left) / num;
//        deltaRightX = (targetRectF.right - currRectF.right) / num;
//        deltaTriangleX = (targetTrianglePoints[0].x - currTrianglePoints[0].x) / num;
//        deltaVoiceX = (targetVoiceRectF.left - currVoiceRectF.left) / num;
//        deltaVoiceY = (targetVoiceRectF.top - currVoiceRectF.top) / num;
//        invalidate();
    }
}

package com.huolongluo.wechatvioce.widget2;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.huolongluo.wechatvioce.R;
import com.huolongluo.wechatvioce.util.DisplayUtil;
import com.huolongluo.wechatvioce.widget.VoiceBottomArc;

public class VoiceView2 extends FrameLayout {
    private String TAG = "WeChatVoiceView";
    private VoiceBottomArc weChatVoiceBottomArcLight;
    private VoiceBottomArc weChatVoiceBottomArcDark;
    private final int ANIM_DURATION = 300;
    private final int ANIM_DURATION_TEXT = 500;
    private final int ANIM_DURATION_TEXT_BIGGER = 100;
    private ImageView voiceIv;
    private int bottomArcTransY;
    private TextView voiceTv;
    private TextView cancelTv;
    private TextView translateTv;
    private ObjectAnimator darkAlphaAnim;
    private ObjectAnimator lightAlphaAnim;
    boolean currentArcLight = true;
    boolean lightAniming = false;
    boolean darkAniming = false;
    private AnimatorSet bottomArcSet;
    private AnimatorSet textAnimSet;
    private int[] screenWH;
    private int screenWidth;//屏幕宽度
    private int screenHeight;//屏幕高度
    private AnimatorSet cancelTvScaleBiggerAnim;
    private AnimatorSet translateTvScaleBiggerAnim;
    private AnimatorSet cancelTvScaleSmallAnim;
    private AnimatorSet translateTvScaleSmallAnim;
    private TextView cancelHintTv;
    private TextView translateHintTv;
    private VoiceBubble2 voice_bubble2;

    public VoiceView2(@NonNull Context context) {
        this(context, null);
    }

    public VoiceView2(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VoiceView2(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_voice2, this);
        initView();
        initAnimation();
    }

    private void initView() {
        voiceIv = findViewById(R.id.iv_voice);
        weChatVoiceBottomArcLight = findViewById(R.id.bottom_arc_light);
        weChatVoiceBottomArcDark = findViewById(R.id.bottom_arc_dark);
        voice_bubble2 = findViewById(R.id.voice_bubble2);
        cancelTv = findViewById(R.id.tv_cancel);
        translateTv = findViewById(R.id.tv_trans);
        bottomArcTransY = getResources().getDimensionPixelOffset(R.dimen.arc_height_light);
        voiceTv = findViewById(R.id.voice);
        cancelHintTv = findViewById(R.id.tv_cancel_text);
        translateHintTv = findViewById(R.id.tv_trans_text);
        screenWH = DisplayUtil.getHW(getContext());
        screenWidth = screenWH[0];
        screenHeight = screenWH[1];
    }

    private void initAnimation() {
        ObjectAnimator bottomArcTransYAnim = ObjectAnimator.ofFloat(weChatVoiceBottomArcLight, "translationY", bottomArcTransY, 0);
        ObjectAnimator bottomArcAlphaAnim = ObjectAnimator.ofFloat(weChatVoiceBottomArcLight, "alpha", 0f, 1f);
        ObjectAnimator voiceTvTransYAnim = ObjectAnimator.ofFloat(voiceTv, "translationY", 300, 0);
        ObjectAnimator voiceTvAlphaAnim = ObjectAnimator.ofFloat(voiceTv, "alpha", 0f, 1f);
        weChatVoiceBottomArcLight.setVisibility(View.VISIBLE);
        bottomArcSet = new AnimatorSet();
        bottomArcSet.playTogether(bottomArcTransYAnim, bottomArcAlphaAnim, voiceTvTransYAnim, voiceTvAlphaAnim);
        bottomArcSet.setDuration(ANIM_DURATION);
        bottomArcSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                weChatVoiceBottomArcDark.setVisibility(View.VISIBLE);
            }
        });
        ObjectAnimator cancelTvTransYAnim = ObjectAnimator.ofFloat(cancelTv, "translationY", 100, 0);
        ObjectAnimator cancelTvAlphaAnim = ObjectAnimator.ofFloat(cancelTv, "alpha", 0f, 1f);
        ObjectAnimator translateTvTransYAnim = ObjectAnimator.ofFloat(translateTv, "translationY", 100, 0);
        ObjectAnimator translateTvAlphaAnim = ObjectAnimator.ofFloat(translateTv, "alpha", 0f, 1f);
        textAnimSet = new AnimatorSet();
        textAnimSet.playTogether(cancelTvTransYAnim, cancelTvAlphaAnim, translateTvTransYAnim, translateTvAlphaAnim);
        textAnimSet.setDuration(ANIM_DURATION_TEXT);

        darkAlphaAnim = ObjectAnimator.ofFloat(weChatVoiceBottomArcLight, "alpha", 1f, 0f);
        darkAlphaAnim.setDuration(100);
        darkAlphaAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                darkAniming = false;
                currentArcLight = false;
                voiceIv.setImageResource(R.mipmap.ic_voice_dark);
            }
        });

        lightAlphaAnim = ObjectAnimator.ofFloat(weChatVoiceBottomArcLight, "alpha", 0f, 1f);
        lightAlphaAnim.setDuration(100);
        lightAlphaAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                lightAniming = false;
                currentArcLight = true;
                voiceIv.setImageResource(R.mipmap.ic_voice);
            }
        });

        float src = 1f, tar = 1.2f;
        ObjectAnimator cancelTvScaleXBiggerAnim = ObjectAnimator.ofFloat(cancelTv, "scaleX", src, tar);
        ObjectAnimator cancelTvScaleYBiggerAnim = ObjectAnimator.ofFloat(cancelTv, "scaleY", src, tar);
        ObjectAnimator cancelTvScaleXSmallAnim = ObjectAnimator.ofFloat(cancelTv, "scaleX", tar, src);
        ObjectAnimator cancelTvScaleYSmallAnim = ObjectAnimator.ofFloat(cancelTv, "scaleY", tar, src);

        cancelTvScaleBiggerAnim = new AnimatorSet();
        cancelTvScaleBiggerAnim.playTogether(cancelTvScaleXBiggerAnim, cancelTvScaleYBiggerAnim);
        cancelTvScaleBiggerAnim.setDuration(ANIM_DURATION_TEXT_BIGGER);
        cancelTvScaleBiggerAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                cancelTvAnimationing = false;
                cancelTvBig = true;
            }
        });
        cancelTvScaleSmallAnim = new AnimatorSet();
        cancelTvScaleSmallAnim.playTogether(cancelTvScaleXSmallAnim, cancelTvScaleYSmallAnim);
        cancelTvScaleSmallAnim.setDuration(ANIM_DURATION_TEXT_BIGGER);
        cancelTvScaleSmallAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                cancelTvAnimationing = false;
                cancelTvBig = false;
            }
        });

        ObjectAnimator translateTvScaleXBiggerAnim = ObjectAnimator.ofFloat(translateTv, "scaleX", src, tar);
        ObjectAnimator translateTvScaleYBiggerAnim = ObjectAnimator.ofFloat(translateTv, "scaleY", src, tar);
        ObjectAnimator translateTvScaleXSmallAnim = ObjectAnimator.ofFloat(translateTv, "scaleX", tar, src);
        ObjectAnimator translateTvScaleYSmallAnim = ObjectAnimator.ofFloat(translateTv, "scaleY", tar, src);

        translateTvScaleBiggerAnim = new AnimatorSet();
        translateTvScaleBiggerAnim.playTogether(translateTvScaleXBiggerAnim, translateTvScaleYBiggerAnim);
        translateTvScaleBiggerAnim.setDuration(ANIM_DURATION_TEXT_BIGGER);
        translateTvScaleBiggerAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                translateTvAnimationing = false;
                translateTvBig = true;
            }
        });
        translateTvScaleSmallAnim = new AnimatorSet();
        translateTvScaleSmallAnim.playTogether(translateTvScaleXSmallAnim, translateTvScaleYSmallAnim);
        translateTvScaleSmallAnim.setDuration(ANIM_DURATION_TEXT_BIGGER);
        translateTvScaleSmallAnim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation, boolean isReverse) {
                translateTvAnimationing = false;
                translateTvBig = false;
            }
        });
    }


    private void startAnim() {
        bottomArcSet.start();
        textAnimSet.start();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    public void doStart() {
        new Handler(Looper.getMainLooper()).post(this::startAnim);
    }

    public void doDefault() {
        weChatVoiceBottomArcLight.setTranslationY(bottomArcTransY);
        weChatVoiceBottomArcDark.setVisibility(View.GONE);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true;
    }

    String showStr = "简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao！！！CCC";

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        float rawX = ev.getRawX();//触摸点距离屏幕左边界的距离
        float rawY = ev.getRawY();//触摸点距离屏幕上边界的距离
        float x = ev.getX();
        float y = ev.getY();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
//                Log.e(TAG, "onTouch: 按下");
                tryChangeToLight();
                tryChangeCancelTextToSmall();
                tryChangeTranslateTextToSmall();
                voiceTv.setVisibility(View.VISIBLE);
                voice_bubble2.setShowType(VoiceBubble2.SHOW_TYPE.TYPE_CENTER, "简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao！！！AAA");
                break;
            case MotionEvent.ACTION_MOVE:
//                Log.e(TAG, "onTouch: 移动");
                int resultHeight = screenHeight - DisplayUtil.dip2px(getContext(), 150);
                if (rawY > resultHeight) {
                    tryChangeToLight();
                    tryChangeCancelTextToSmall();
                    tryChangeTranslateTextToSmall();
                    voiceTv.setVisibility(View.VISIBLE);
                    voice_bubble2.setShowType(VoiceBubble2.SHOW_TYPE.TYPE_CENTER, "简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao！！！AAA");
//            Log.e(TAG, "onTouchEvent: 在区域内，触摸点距离屏幕左边x：" + x + "  触摸点距离屏幕上边y：" + y);
                } else {
                    // 不在区域里，看在屏幕左边右边
                    tryChangeToDark();
                    if (rawX < screenWidth / 2) {
                        tryChangeCancelTextToBigger();
                        tryChangeTranslateTextToSmall();
                        voice_bubble2.setShowType(VoiceBubble2.SHOW_TYPE.TYPE_CANCEL, "简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao简书YocnZhao！！！BBB");
                    } else {
                        showStr += "触摸反馈";
                        tryChangeTranslateTextToBigger();
                        tryChangeCancelTextToSmall();
                        voice_bubble2.setShowType(VoiceBubble2.SHOW_TYPE.TYPE_TRANSLATE, showStr);
                    }
                    voiceTv.setVisibility(View.GONE);
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                Log.e(TAG, "onTouch: 抬起");
                setVisibility(GONE);
                break;
        }


        Log.e(TAG, "onTouchEvent: 是否在区域内：" + weChatVoiceBottomArcLight.isOnRect(x, y) + " x：" + x + "  y：" + y);
        return true;
    }

    private void setTextBigger(TextView text) {
        text.setTextColor(getResources().getColor(R.color.black));
        text.setBackgroundResource(R.drawable.bg_trans_oval_white);
    }

    private void setTextSmall(TextView text) {
        text.setTextColor(getResources().getColor(R.color.white));
        text.setBackgroundResource(R.drawable.bg_trans_oval);
    }

    boolean cancelTvBig = false;
    boolean translateTvBig = false;
    boolean cancelTvAnimationing = false;
    boolean translateTvAnimationing = false;

    private void tryChangeCancelTextToBigger() {
        if (cancelTvBig || cancelTvAnimationing) {
            return;
        }
        cancelTvAnimationing = true;
        setTextBigger(cancelTv);
        cancelHintTv.setVisibility(View.VISIBLE);
        cancelTvScaleBiggerAnim.start();
    }

    private void tryChangeCancelTextToSmall() {
        if (!cancelTvBig || cancelTvAnimationing) {
            return;
        }
        cancelHintTv.setVisibility(View.GONE);
        cancelTvAnimationing = true;
        setTextSmall(cancelTv);
        cancelTvScaleSmallAnim.start();
    }

    private void tryChangeTranslateTextToBigger() {
        if (translateTvBig || translateTvAnimationing) {
            return;
        }
        translateHintTv.setVisibility(View.VISIBLE);
        translateTvAnimationing = true;
        setTextBigger(translateTv);
        translateTvScaleBiggerAnim.start();
    }

    private void tryChangeTranslateTextToSmall() {
        if (!translateTvBig || translateTvAnimationing) {
            return;
        }
        translateHintTv.setVisibility(View.GONE);
        translateTvAnimationing = true;
        setTextSmall(translateTv);
        translateTvScaleSmallAnim.start();
    }

    private void tryChangeToDark() {
        if (!currentArcLight || darkAniming) {
            return;
        }
        darkAniming = true;
        darkAlphaAnim.start();
    }

    private void tryChangeToLight() {
        if (currentArcLight || lightAniming) {
            return;
        }
        lightAniming = true;
        lightAlphaAnim.start();
    }

}
